﻿using System.Linq;

namespace Studi_Clinici
{
    partial class login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lUOGOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.studiCliniciDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.studiCliniciDataSet = new Studi_Clinici.studiCliniciDataSet();
            this.lUOGOTableAdapter = new Studi_Clinici.studiCliniciDataSetTableAdapters.LUOGOTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lUOGOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studiCliniciDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studiCliniciDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(89, 97);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 33);
            this.button1.TabIndex = 1;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.lUOGOBindingSource, "nome", true));
            this.comboBox1.DisplayMember = "nome";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 41);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(258, 24);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.ValueMember = "nome";
            // 
            // lUOGOBindingSource
            // 
            this.lUOGOBindingSource.DataMember = "LUOGO";
            this.lUOGOBindingSource.DataSource = this.studiCliniciDataSetBindingSource;
            // 
            // studiCliniciDataSetBindingSource
            // 
            this.studiCliniciDataSetBindingSource.DataSource = this.studiCliniciDataSet;
            this.studiCliniciDataSetBindingSource.Position = 0;
            // 
            // studiCliniciDataSet
            // 
            this.studiCliniciDataSet.DataSetName = "studiCliniciDataSet";
            this.studiCliniciDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lUOGOTableAdapter
            // 
            this.lUOGOTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Selezione Ospedale";
            // 
            // login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 158);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button1);
            this.Name = "login";
            this.Text = "login";
            this.Load += new System.EventHandler(this.login_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lUOGOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studiCliniciDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studiCliniciDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.BindingSource studiCliniciDataSetBindingSource;
        private studiCliniciDataSet studiCliniciDataSet;
        private System.Windows.Forms.BindingSource lUOGOBindingSource;
        private studiCliniciDataSetTableAdapters.LUOGOTableAdapter lUOGOTableAdapter;
        private System.Windows.Forms.Label label1;
    }
}