﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studi_Clinici
{
    public partial class RicercaPazienti : Form
    {
        public RicercaPazienti()
        {
            InitializeComponent();
        }

        String nome;
        String stadio;
        String sede;

        private void button1_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            IQueryable q;
            
            if (string.IsNullOrEmpty(sede) || string.IsNullOrEmpty(stadio))
            {
                q =
                from s in db.STUDIO
                join p in db.PATOLOGIA on s.codice_patologia equals p.codice_patologia
                join pr in db.presso on s.codice_studio equals pr.codice_studio
                join l in db.LUOGO on pr.codice_luogo equals l.codice_luogo
                where p.nome == nome
                select new
                {
                    Codice = s.codice_studio,
                    Studio = s.nome,
                    Acronimo = s.acronimo,
                    Città = l.ind_citta,
                    Ospedale = l.nome,
                    Recapito = l.recapito
                }
                ;
            } else
            {
                q =
                from s in db.STUDIO
                join p in db.PATOLOGIA on s.codice_patologia equals p.codice_patologia
                join pr in db.presso on s.codice_studio equals pr.codice_studio
                join l in db.LUOGO on pr.codice_luogo equals l.codice_luogo
                where p.nome == nome
                where p.stadio == stadio
                where p.sede == sede
                select new
                {   
                    Codice = s.codice_studio,
                    Studio = s.nome,
                    Acronimo = s.acronimo,
                    Città = l.ind_citta,
                    Ospedale = l.nome,
                    Recapito = l.recapito
                }
                ;
            }

            dataGridView1.DataSource = q;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            nome = (sender as TextBox).Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            sede = (sender as TextBox).Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            stadio = (sender as TextBox).Text;
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var value = dataGridView1[0, e.RowIndex].Value.ToString();
            Form ins = new InserisciPaziente(value);
            ins.Show();
        }
    }
}
