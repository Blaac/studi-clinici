﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studi_Clinici
{
    public partial class RicercaPazientiLuogo : Form
    {
        public RicercaPazientiLuogo()
        {
            InitializeComponent();
        }

        String nome;
        String stadio;
        String sede;
        String citta;

        private void button1_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            IQueryable q;
            
            if (string.IsNullOrEmpty(sede) || string.IsNullOrEmpty(stadio))
            {
                q =
                from pr in db.presso
                where
                    pr.STUDIO.PATOLOGIA.nome == nome &&
                    pr.LUOGO.ind_citta == citta
                select new
                {
                    Studio = pr.STUDIO.nome,
                    Acronimo = pr.STUDIO.acronimo,
                    Ospedale = pr.LUOGO.nome,
                    Recapito = pr.LUOGO.recapito
                };
            } else
            {
                q =
                from pr in db.presso
                where
                    pr.STUDIO.PATOLOGIA.nome == nome &&
                    pr.STUDIO.PATOLOGIA.stadio == stadio &&
                    pr.STUDIO.PATOLOGIA.sede == sede &&
                    pr.LUOGO.ind_citta == citta
                select new
                {
                    Studio = pr.STUDIO.nome,
                    Acronimo = pr.STUDIO.acronimo,
                    Ospedale = pr.LUOGO.nome,
                    Recapito = pr.LUOGO.recapito
                };
                
            }

            dataGridView1.DataSource = q;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            nome = (sender as TextBox).Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            sede = (sender as TextBox).Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            stadio = (sender as TextBox).Text;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            citta = (sender as TextBox).Text;
        }
    }
}
