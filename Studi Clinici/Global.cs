﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studi_Clinici
{
    static class Global
    {
        private static string _ospedale = "";

        public static string Ospedale
        {
            get { return _ospedale; }
            set { _ospedale = value; }
        }
    }
}
