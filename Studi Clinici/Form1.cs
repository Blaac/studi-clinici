﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studi_Clinici
{
    public partial class Form1 : Form
    {
        
        
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBox cbx = (sender as ComboBox);
            var item = cbx.SelectedItem;
            if (item.Equals("Ricerca per patologia"))
            {
                Form ricPat = new RicercaPazienti();
                ricPat.Show();
            } else if (item.Equals("Ricerca per patologia e luogo")){
                Form ricPatL = new RicercaPazientiLuogo();
                ricPatL.Show();
            } else if (item.Equals("Visualizza studi attivi in questo ospedale"))
            {
                Form studL = new StudiLuogo();
                studL.Show();
            }
                
         }

        

        private void button1_Click(object sender, EventArgs e)
        {
            Global.Ospedale = "";
            this.Hide();
            login lg = new login();
            lg.Show();
        }
    }
}
