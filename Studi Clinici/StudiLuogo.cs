﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studi_Clinici
{
    public partial class StudiLuogo : Form
    {
        public StudiLuogo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();

            IQueryable q = from pr in db.presso
                           where
                             pr.LUOGO.nome == Global.Ospedale
                           select new
                           {
                               Nome = pr.STUDIO.nome,
                               Acronimo = pr.STUDIO.acronimo,
                               NumeroPazienti = pr.STUDIO.num_pazienti,
                               Inizio = pr.STUDIO.data_apertura,
                               Fine = pr.STUDIO.data_chiusura
                           };
            dataGridView1.DataSource = q;
        }
    }
}
