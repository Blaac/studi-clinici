﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studi_Clinici
{
    public partial class InserisciPaziente : Form
    {
        string cod;
        public InserisciPaziente(string _cod)
        {
            InitializeComponent();
            cod = _cod;
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            textBox1.Text = cod;
        }
    }
}
