﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Studi_Clinici
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Global.Ospedale = comboBox1.Text;
            this.Hide();
            Form1 frm = new Form1();
            frm.Show();
        }

        private void login_Load(object sender, EventArgs e)
        {
            DataClasses1DataContext db = new DataClasses1DataContext();
            IQueryable q = from l in db.LUOGO select new { l.nome };
            this.comboBox1.DataSource = q;



        }
    }
}
